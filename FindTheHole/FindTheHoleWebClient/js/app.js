// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic']);

app.run(function($rootScope) {
    $rootScope.choosenHole = "seleziona un'aula";
    //array che contiene il nome di tutte le aule, viene usato per scegliere una specifica aula
    $rootScope.allHolesName = [];
    //aula selezionata per la ricerca
    $rootScope.selectedSpecificHole = "";
    //i feedback devono essere visualizzati al primo avvio
    $rootScope.startApplicationFeedBack = true;

    //messaggio da visualizzare nel caricamento dei dati
    $rootScope.messageLoading = "Ricerca aule..."
    $rootScope.isAndroidDevice = false;
    $rootScope.showAndroidDeviceAnotherTime = true;

})



app.filter('filterSpecificHole', function() {
    return function(input) {
        return input != undefined ? 'Orario lezione: '+input : 'Nessuna lezione prevista per oggi';
    }
})

app.filter('feedbackFilter', function() {
    return function(input) {
        return input == true ? 'Libera' : 'Occupata';
    }
})

app.filter('getHoursAndMinuteFromDate', function() {
    return function(input) {
        var date = new Date(input)
        return date.getHours()+":"+(date.getMinutes()<10?'0':'') + date.getMinutes();
    }
})

app.config(function($stateProvider,$urlRouterProvider) {
  $stateProvider
      .state('Find The Hole', {
        url: '/',
        templateUrl: 'home.html'
      }).state('about', {
          url: '/',
          templateUrl: 'about.html'
      }).state('holes', {
          url: '/',
          templateUrl: 'holes.html'
      }).state('segnalaAula', {
        url: '/',
        templateUrl: 'segnalaAula.html'
    })
    $urlRouterProvider.otherwise("/")
});


app.constant('$ionicLoadingConfig', {
  content: '<i class=" ion-loading-c"></i> ',
  animation: 'fade-in',
  // Will a dark overlay or backdrop cover the entire view
  showBackdrop: true,

  // The maximum width of the loading indicator
  // Text will be wrapped if longer than maxWidth
  maxWidth: 200,
  // The delay in showing the indicator
  showDelay: 0
});


app.controller('SecondViewController', function($scope, $http,$ionicLoading,$ionicPopup,filterFilter,$ionicNavBarDelegate,$rootScope,$ionicModal) {

   /* $ionicLoading.show({ template: 'Item Added!', noBackdrop: true, duration: 2000 });*/

    $scope.data="";
    $scope.sendStatusHole = {}
    $scope.sendStatusHole.hole = ""
    $scope.sendStatusHole.status = ""

    $scope.filteredArray =  $rootScope.allHolesName.slice();

    $scope.emptySearchHole = function(){
        document.getElementById("searchHolesBar").value = "";
        $scope.data=""
        $scope.filteredArray = filterFilter($rootScope.allHolesName, {nome:""});
    }

    //Salva l'aula selezionata per la ricerca specifica
    $scope.sendHoles = function(name){
        $rootScope.selectedSpecificHole=name;
    }
    $scope.myGoBack = function() {
        $ionicNavBarDelegate.back();
    };

    $scope.filterHolesName=function(data){

        $scope.filteredArray = filterFilter($rootScope.allHolesName, {nome:data});
    };

    $scope.$on('$viewContentLoaded', function() {

        if ($rootScope.allHolesName.length == 0) {
            $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca aule '});
            var result = call_services.findAllHolesName($http, function (result) {
                $ionicLoading.hide();
                if (result) {
                    $rootScope.allHolesName = [];
                    for (var i = 0; i < result.holes.length; i++) {
                        $rootScope.allHolesName.push({'nome': result.holes[i].nome});
                        $scope.filteredArray.push({'nome': result.holes[i].nome});
                    }
                    return true;
                }
                else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Errore!',
                        template: 'Impossibile contattare il server'
                    });
                    return false;
                }
            })
        }

    });


    $scope.showConfirm = function() {
        var confirmPopup = $ionicPopup.show({
            title: "Stato dell'aula",

            template: "<label>Seleziona stato aula<label></label></label><div class='list'><label class='item item-radio'><input type='radio' name='group' id='libera'><div class='item-content'>Libera"+
        "</div><i class='radio-icon ion-checkmark'></i></label><label class='item item-radio'><input ng-model='sendStatusHole.status' type='radio' name='group'><div class='item-content'>Occupata"+
            "</div><i class='radio-icon ion-checkmark'></i></label></div>",
            buttons: [{text:'Cancella'},{text:'Invia', onTap: function(e) {
                if (document.getElementById("libera").checked) {
                    //don't allow the user to close unless he enters wifi password
                    $scope.sendStatusHole.status = true
                    return true
                } else {
                    $scope.sendStatusHole.status = false
                    return true
                }}}]

        });
        confirmPopup.then(function(res) {
            if(res) {
                $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Invio segnalazione '});
                var currentdate = new Date();
                var hours = currentdate.getHours();
                var minutes = currentdate.getMinutes();

                if (hours<10) hours = "0" + hours;
                if (minutes<10) minutes = "0" + minutes;

                var time = ""+ hours + ":" + minutes;
                var datetime =time;
                call_services.sendHoleFeedBack($scope.sendStatusHole,datetime,$http, function(data){
                    $ionicLoading.hide();
                    if(data){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Segnalazione Aula',
                            template: "Segnalazione salvata"
                        });
                        alertPopup.then(function (res) {
                            $ionicNavBarDelegate.back()
                        });

                    }else{
                        var alertPopup = $ionicPopup.alert({
                            title: 'Segnalazione Aula',
                            template: "Errore salvataggio segnalazione."
                        });
                        alertPopup.then(function (res) {

                        });
                    }
                })
            } else {
                console.log('You are not sure');
            }
        });
    };

    //inivia la segnalazione di un'aula libera
    $scope.sendFreeHoles = function(hole){
        $scope.sendStatusHole.hole = hole;
        $scope.showConfirm(hole);
    }






});

app.controller('FindHolesController', function($scope, $http,$ionicLoading,$ionicPopup,$rootScope) {


    $scope.signedeHoles = false;
    $scope.feedback = [];


    $scope.closeDownload = function(){
        $rootScope.isAndroidDevice = false;
    }


    $scope.getHolesFeedBack = function(){

        $scope.signedeHoles = true;
        $scope.selectedHole = false;
        $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca segnalazioni '});
        call_services.getHolesFeedBack($http,function(data){
            $ionicLoading.hide();
            if(data){
                $scope.feedback = data.feedback;
            }else{
                var alertPopup = $ionicPopup.alert({
                    title: 'Errore!',
                    template: 'Impossibile ottenere i dati'
                });
            }
        })
    }


    $scope.$on('$viewContentLoaded', function() {

        if ($rootScope.startApplicationFeedBack) {
           $scope.getHolesFeedBack()
        }

    });

     //Visualizza poupiniziale
     if( window.localStorage['showInitialPopup']!='false'){
         var alertPopup = $ionicPopup.alert({
             title: 'Benvenuto!',
             template: "ATTENZIONE: Ricorda che gli orari delle lezioni sono quelli corrispondenti agli orari pubblicati sui diversi siti della facolta' di scienze. Gli sviluppatori non "+
             "si assumono alcuna responsabilita' riguardo eventuali modifiche di tali orari."
         });
         alertPopup.then(function (res) {
             console.log('Thank you for not eating my delicious ice cream cone');
             window.localStorage['showInitialPopup'] = 'false';

            window.localStorage['showInitialPopup'] || 'you';


         });
     }
    if( /Android/i.test(navigator.userAgent) && $rootScope.showAndroidDeviceAnotherTime) {

        $rootScope.isAndroidDevice = true;
        $rootScope.showAndroidDeviceAnotherTime = false;
    }

    //visualizza info sull'aula cliccata
    $scope.showInfoHole = function(lezione){
        var alertPopup = $ionicPopup.alert({
            title: 'Info!',
            template:  'Aula libera: '+ lezione.aula+'<br> Prossima lezione: '+lezione.prossima_lezione
        });
        alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
        });
    }


    //visualizza info sull'aula cliccata
    $scope.showInfoFromSpecificHole = function(orari){
        var alertPopup = $ionicPopup.alert({
            title: 'Info!',
            template: orari.orario != undefined ? 'Orario lezione: '+orari.orario : 'Nessuna lezione prevista per oggi'
        });
        alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
        });
    }

    $scope.findSpecificHole = function(hole){
        $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca lezioni '})
        $rootScope.startApplicationFeedBack = false
        $scope.signedeHoles = false
        $scope.selectedHole = true
        call_services.searchLessonOnSpecificHole(hole,$http,function(result){
            $ionicLoading.hide();
            if (result.zeroResults == undefined) {
                $scope.second_search = false;
                //ocument.getElementById("button_aule_libere").innerHTML = "Effettua nuova ricerca"
                $scope.singleHolesResult = result;

            }
            else {
                $scope.second_search = true;
                //document.getElementById("button_aule_libere").innerHTML = "Effettua nuova ricerca"
                $scope.singleHolesResult = [{
                    orario : undefined
                }];
            }

        })
    }

    $scope.findHoles = function () {

        $scope.signedeHoles = false
        $scope.selectedHole = false
        $rootScope.selectedSpecificHole = ""
        $rootScope.startApplicationFeedBack = false;
        $scope.message = "Invio richiesta al server"
        $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca aule '});


        var currentdate = new Date();
        var hours = currentdate.getHours();
        var minutes = currentdate.getMinutes();

        if (hours<10) hours = "0" + hours;
        if (minutes<10) minutes = "0" + minutes;

        var time = ""+ hours + ":" + minutes;
        var datetime =time;



            call_services.findHoles(datetime, $http, function (result) {
                $ionicLoading.hide();
                if (result) {
                    $scope.second_search = true;
                    //document.getElementById("button_aule_libere").innerHTML = "Effettua nuova ricerca"
                    $scope.holes = result;
                }
                else {

                    var alertPopup = $ionicPopup.alert({
                        title: 'Errore!',
                        template: 'Impossibile contattare il server'
                    });
                    alertPopup.then(function (res) {
                        console.log('Thank you for not eating my delicious ice cream cone');
                    });

                }
            })

    };

     //varabile che esprime se deve essere cercata un'aula specifica
     $scope.selectedHole = false;
     if($rootScope.selectedSpecificHole != ""){
         $scope.selectedHole = true;
         $scope.findSpecificHole($rootScope.selectedSpecificHole);
     }

     //Lista delle aule trovate
     $scope.holes = [];
     //Lista per la singola aula cercata = []
     $scope.singleHolesResult = [];

     $scope.getItemHeight = function(item, index) {
         //Make evenly indexed items be 10px taller, for the sake of example
         return (index % 1) === 0 ? 50 : 60;
     };

     //rimuove l'aula specificata
     $scope.removeSelectedHole = function(){
         $rootScope.selectedSpecificHole = "";
         $scope.selectedHole = false
     }



 });

app.controller('aboutController', function($scope,$ionicPopup){
    $scope.showLicensePopup = function(){
        var alertPopup = $ionicPopup.alert({
            title: 'Licenza!',
            template: "<p style='text-align: justify'>Copyright 2015 BaraccaSoftware development Inc.Licensed under the Apache License, Version 2.0 (the 'License'); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an 'AS IS' BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.</p>"
        });
        alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
            $rootScope.showInitialPopup = false;
        });
    }

})


      var IP_LOCAL_SERVER="http://127.0.0.1/";
      var IP_SERVER = "http://findthehole-baraccasoftware.rhcloud.com/";
      var IP_TIMEOUT = 7000;

      var call_services = {
          //prendi gli utlimi feedback
          getHolesFeedBack:function($http, callback){
              var url = IP_SERVER + 'getHolesFeedBack?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(true);
              })
          },
          //invia un feedback su un aula
          sendHoleFeedBack : function(status,datetime, $http, callback){
              var url = IP_SERVER + 'sendHoleFeedBack?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {time: datetime,status: status}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(true);
              })
          },
          findHoles: function (datetime, $http, callback) {
              var url = IP_SERVER + 'findHoles?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {time: datetime}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(null);
              })

          },
          searchLessonOnSpecificHole : function(hole,$http,callback){
              var url = IP_SERVER + 'findSpecificHole?callback=JSON_CALLBACK';
              var temp_hole = {
                  aula : hole
              }
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params:  {hole: temp_hole}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(e);
              })
          },
          findAllHolesName: function ($http, callback) {
              var url = IP_SERVER + 'getAllHoles?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url
                  }
              );
              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(null);
              })
          }
      };

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


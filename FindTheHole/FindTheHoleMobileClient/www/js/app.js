// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


var app = angular.module('findTheHoleApp', ['ionic', 'ionic-timepicker','applicationFilters','applicationControllers']);

app.run(function($rootScope) {
    $rootScope.choosenHole = "seleziona un'aula";
    //array che contiene il nome di tutte le aule, viene usato per scegliere una specifica aula
    $rootScope.allHolesName = [];
    //aula selezionata per la ricerca
    $rootScope.selectedSpecificHole = "";
    //i feedback devono essere visualizzati al primo avvio
    $rootScope.startApplicationFeedBack = true;

    //messaggio da visualizzare nel caricamento dei dati
    $rootScope.messageLoading = "Ricerca aule..."

    $rootScope.isAndroidDevice = false;
    $rootScope.showAndroidDeviceAnotherTime = false;

})

app.directive('capitalize', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var capitalize = function(inputValue) {
                if(inputValue == undefined) inputValue = '';
                var capitalized = inputValue.toUpperCase();
                if(capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                return capitalized;
            }
            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]);  // capitalize initial value
        }
    };
});



app.config(function($stateProvider,$urlRouterProvider) {
  $stateProvider
      .state('Find The Hole', {
        url: '/',
        templateUrl: 'home.html'
      }).state('about', {
          url: '/',
          templateUrl: 'about.html'
      }).state('holes', {
          url: '/',
          templateUrl: 'holes.html'
      }).state('segnalaAula', {
        url: '/',
        templateUrl: 'segnalaAula.html'
      }).state('segnalaCambioOrario',{
          url: '/',
          templateUrl: 'changeHourPage.html'
      })
    $urlRouterProvider.otherwise("/")
});


app.constant('$ionicLoadingConfig', {
  content: '<i class=" ion-loading-c"></i> ',
  animation: 'fade-in',
  // Will a dark overlay or backdrop cover the entire view
  showBackdrop: true,

  // The maximum width of the loading indicator
  // Text will be wrapped if longer than maxWidth
  maxWidth: 200,
  // The delay in showing the indicator
  showDelay: 0
});



      var IP_LOCAL_SERVER="http://127.0.0.1/";
      var IP_SERVER = "http://findthehole-baraccasoftware.rhcloud.com/";
      var IP_TIMEOUT = 10000;

      var call_services = {
          sendLessonChange:function($http,holeName,startLesson,endLesson, dayOfWeek,callback){
              var url = IP_SERVER + 'sendLessonChange?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {holeName: holeName,startLesson: startLesson,endLesson: endLesson,dayOfWeek: dayOfWeek}
                  }
              );
              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(true);
              })

            },
          //prendi gli utlimi feedback
          getHolesFeedBack:function($http, callback){
              var url = IP_SERVER + 'getHolesFeedBack?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(true);
              })
          },
          //invia un feedback su un aula
          sendHoleFeedBack : function(status,datetime, $http, callback){
              var url = IP_SERVER + 'sendHoleFeedBack?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {time: datetime,status: status}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(true);
              })
          },
          findHoles: function (datetime, $http, callback) {
              var url = IP_SERVER + 'findHoles?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params: {time: datetime}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(null);
              })

          },
          searchLessonOnSpecificHole : function(hole,$http,callback){
              var url = IP_SERVER + 'findSpecificHole?callback=JSON_CALLBACK';
              var temp_hole = {
                  aula : hole
              }
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url,
                      params:  {hole: temp_hole}
                  }
              );

              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(e);
              })
          },
          findAllHolesName: function ($http, callback) {
              var url = IP_SERVER + 'getAllHoles?callback=JSON_CALLBACK';
              var responsePromise = $http.jsonp(url,
                  {
                      timeout: IP_TIMEOUT,
                      url: url
                  }
              );
              responsePromise.success(function (data) {
                  // do something with the returned JavaScript object
                  // ( in the "data" parameter ).
                  callback(data);
              });
              responsePromise.error(function (e) {
                  callback(null);
              })
          }
      };

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


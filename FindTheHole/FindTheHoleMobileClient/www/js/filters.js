angular.module('applicationFilters', []).
    filter('myDateTimePicker', function() {
    return function(input){

        var currentdate = new Date();
        var time = input.split(":");
        var hours = time[0]
        var minutes = time[1]

        currentdate.setHours(hours-1 % 24);
        currentdate.setMinutes(minutes);
        hours = currentdate.getHours();
        minutes = currentdate.getMinutes();

        if (hours<10) hours = "0" + hours;
        if (minutes<10) minutes = "0" + minutes;

        var time = ""+ hours + ":" + minutes;

        return time
    }
    }).
    filter('filterSpecificHole', function() {
        return function(input) {
            return input != undefined ? 'Orario lezione: '+input : 'Nessuna lezione prevista per oggi';
        }
    }).filter('feedbackFilter', function() {
        return function(input) {
            return input == true ? 'Libera' : 'Occupata';
        }
    }).filter('getHoursAndMinuteFromDate', function() {
        return function(input) {
            var date = new Date(input)
            return date.getHours()+":"+(date.getMinutes()<10?'0':'') + date.getMinutes();
        }
    });
angular.module('applicationControllers', []).controller('changeHourController', function($scope, $http,$filter,$ionicLoading,$ionicPopup,filterFilter,$ionicNavBarDelegate,$rootScope,$ionicModal){

    $scope.holeName=""
    $scope.dayOfWeek= [
        { text: "Lunedì", checked: true },
        { text: "Martedì", checked: false },
        { text: "Mercoledì", checked: false },
        { text: "Giovedì", checked: false },
        { text: "Venerdì", checked: false }
    ];
    $scope.slots = [
        {epochTime: 12600, step: 15, format: 12},
        {epochTime: 12600, step: 15, format: 24}
    ];


    $scope.sendChange = function(holeName){
        $scope.holeName = holeName
        var startLesson = $filter('date')($scope.slots[0].epochTime * 1000, 'H:mm')
        startLesson = $filter('myDateTimePicker')(startLesson);

        var endLesson = $filter('date')($scope.slots[1].epochTime * 1000, 'H:mm')
        endLesson = $filter('myDateTimePicker')(startLesson);

       if( $scope.holeName =="" || $scope.holeName.length <= 0 || $scope.holeName ==" "){
           // An alert dialog
           $ionicPopup.alert({
               title: 'Attenzione',
               template: 'Il nome dell\'aula non deve essere vuoto'
           });
       }else{
           $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Invio orario '});
           var result = call_services.sendLessonChange($http,$scope.holeName,startLesson,endLesson, $scope.dayOfWeek, function (result) {
               $ionicLoading.hide();
               if(result.message){
                   $ionicPopup.alert({
                       title: 'Info',
                       template: 'Segnalazione inviata correttamente'
                   })
               }else{
                   $ionicPopup.alert({
                       title: 'Attenzione',
                       template: 'Si è verificato un errore'
                   })
               }
           })
       }
    }


    $ionicModal.fromTemplateUrl('my-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function() {
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });

    $scope.saveModal = function() {
        $scope.modal.hide();
    };
}).controller('SecondViewController', function($scope, $http,$ionicLoading,$ionicPopup,filterFilter,$ionicNavBarDelegate,$rootScope,$ionicModal) {

        /* $ionicLoading.show({ template: 'Item Added!', noBackdrop: true, duration: 2000 });*/

        $scope.data="";
        $scope.sendStatusHole = {}
        $scope.sendStatusHole.hole = ""
        $scope.sendStatusHole.status = ""

        $scope.filteredArray =  $rootScope.allHolesName.slice();


        $scope.emptySearchHole = function(){
            document.getElementById("searchHolesBar").value = "";
            $scope.data=""
            $scope.filteredArray = filterFilter($rootScope.allHolesName, {nome:""});
        }

        //Salva l'aula selezionata per la ricerca specifica
        $scope.sendHoles = function(name){
            $rootScope.selectedSpecificHole=name;
        }
        $scope.myGoBack = function() {
            $ionicNavBarDelegate.back();
        };

        $scope.filterHolesName=function(data){

            $scope.filteredArray = filterFilter($rootScope.allHolesName, {nome:data});
        };

        $scope.$on('$viewContentLoaded', function() {

            if ($rootScope.allHolesName.length == 0) {
                $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca aule '});
                var result = call_services.findAllHolesName($http, function (result) {
                    $ionicLoading.hide();
                    if (result) {
                        $rootScope.allHolesName = [];
                        for (var i = 0; i < result.holes.length; i++) {
                            $rootScope.allHolesName.push({'nome': result.holes[i].nome});
                            $scope.filteredArray.push({'nome': result.holes[i].nome});
                        }
                        return true;
                    }
                    else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Errore!',
                            template: 'Impossibile contattare il server'
                        });
                        return false;
                    }
                })
            }

        });


        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.show({
                title: "Stato dell'aula",

                template: "<label>Seleziona stato aula<label></label></label><div class='list'><label class='item item-radio'><input type='radio' name='group' id='libera'><div class='item-content'>Libera"+
                "</div><i class='radio-icon ion-checkmark'></i></label><label class='item item-radio'><input ng-model='sendStatusHole.status' type='radio' name='group'><div class='item-content'>Occupata"+
                "</div><i class='radio-icon ion-checkmark'></i></label></div>",
                buttons: [{text:'Cancella'},{text:'Invia', onTap: function(e) {
                    if (document.getElementById("libera").checked) {
                        //don't allow the user to close unless he enters wifi password
                        $scope.sendStatusHole.status = true
                        return true
                    } else {
                        $scope.sendStatusHole.status = false
                        return true
                    }}}]

            });
            confirmPopup.then(function(res) {
                if(res) {
                    $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Invio segnalazione '});
                    var currentdate = new Date();
                    var hours = currentdate.getHours();
                    var minutes = currentdate.getMinutes();

                    if (hours<10) hours = "0" + hours;
                    if (minutes<10) minutes = "0" + minutes;

                    var time = ""+ hours + ":" + minutes;
                    var datetime =time;
                    call_services.sendHoleFeedBack($scope.sendStatusHole,datetime,$http, function(data){
                        $ionicLoading.hide();
                        if(data){
                            var alertPopup = $ionicPopup.alert({
                                title: 'Segnalazione Aula',
                                template: "Segnalazione salvata"
                            });
                            alertPopup.then(function (res) {
                                $ionicNavBarDelegate.back()
                            });

                        }else{
                            var alertPopup = $ionicPopup.alert({
                                title: 'Segnalazione Aula',
                                template: "Errore salvataggio segnalazione."
                            });
                            alertPopup.then(function (res) {

                            });
                        }
                    })
                } else {
                    console.log('You are not sure');
                }
            });
        };

        //inivia la segnalazione di un'aula libera
        $scope.sendFreeHoles = function(hole){
            $scope.sendStatusHole.hole = hole;
            $scope.showConfirm(hole);
        }






    }).controller('FindHolesController', function($scope, $http,$ionicLoading,$ionicPopup,$rootScope) {


    $scope.signedeHoles = false;
    $scope.feedback = [];


    $scope.closeDownload = function(){
        $rootScope.isAndroidDevice = false;
    }


    $scope.getHolesFeedBack = function(){

        $scope.signedeHoles = true;
        $scope.selectedHole = false;
        $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca segnalazioni '});
        call_services.getHolesFeedBack($http,function(data){
            $ionicLoading.hide();
            if(data){
                $scope.feedback = data.feedback;
            }else{
                var alertPopup = $ionicPopup.alert({
                    title: 'Errore!',
                    template: 'Impossibile ottenere i dati'
                });
            }
        })
    }


    $scope.$on('$viewContentLoaded', function() {

        if ($rootScope.startApplicationFeedBack) {
            $scope.getHolesFeedBack()
        }

    });

    //Visualizza poupiniziale
    if( window.localStorage['showInitialPopup']!='false'){
        var alertPopup = $ionicPopup.alert({
            title: 'Benvenuto!',
            template: "ATTENZIONE: Ricorda che gli orari delle lezioni sono quelli corrispondenti agli orari pubblicati sui diversi siti della facolta' di scienze. Gli sviluppatori non "+
            "si assumono alcuna responsabilita' riguardo eventuali modifiche di tali orari."
        });
        alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
            window.localStorage['showInitialPopup'] = 'false';

            window.localStorage['showInitialPopup'] || 'you';


        });
    }

    if( /Android/i.test(navigator.userAgent) && $rootScope.showAndroidDeviceAnotherTime) {

        $rootScope.isAndroidDevice = true;
        $rootScope.showAndroidDeviceAnotherTime = false;
    }

    //visualizza info sull'aula cliccata
    $scope.showInfoHole = function(lezione){
        var alertPopup = $ionicPopup.alert({
            title: 'Info!',
            template:  'Aula libera: '+ lezione.aula+'<br> Prossima lezione: '+lezione.prossima_lezione
        });
        alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
        });
    }


    //visualizza info sull'aula cliccata
    $scope.showInfoFromSpecificHole = function(orari){
        var alertPopup = $ionicPopup.alert({
            title: 'Info!',
            template: orari.orario != undefined ? 'Orario lezione: '+orari.orario : 'Nessuna lezione prevista per oggi'
        });
        alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
        });
    }

    $scope.findSpecificHole = function(hole){
        $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca lezioni '})
        $rootScope.startApplicationFeedBack = false
        $scope.signedeHoles = false
        $scope.selectedHole = true
        call_services.searchLessonOnSpecificHole(hole,$http,function(result){
            $ionicLoading.hide();
            if (result.zeroResults == undefined) {
                $scope.second_search = false;
                //ocument.getElementById("button_aule_libere").innerHTML = "Effettua nuova ricerca"
                $scope.singleHolesResult = result;

            }
            else {
                $scope.second_search = true;
                //document.getElementById("button_aule_libere").innerHTML = "Effettua nuova ricerca"
                $scope.singleHolesResult = [{
                    orario : undefined
                }];
            }

        })
    }

    $scope.findHoles = function () {

        $scope.signedeHoles = false
        $scope.selectedHole = false
        $rootScope.selectedSpecificHole = ""
        $rootScope.startApplicationFeedBack = false;
        $scope.message = "Invio richiesta al server"
        $ionicLoading.show({content: '<i class=" ion-loading-c"></i> Ricerca aule '});


        var currentdate = new Date();
        var hours = currentdate.getHours();
        var minutes = currentdate.getMinutes();

        if (hours<10) hours = "0" + hours;
        if (minutes<10) minutes = "0" + minutes;

        var time = ""+ hours + ":" + minutes;
        var datetime =time;



        call_services.findHoles(datetime, $http, function (result) {
            $ionicLoading.hide();
            if (result) {
                $scope.second_search = true;
                //document.getElementById("button_aule_libere").innerHTML = "Effettua nuova ricerca"
                $scope.holes = result;
            }
            else {

                var alertPopup = $ionicPopup.alert({
                    title: 'Errore!',
                    template: 'Impossibile contattare il server'
                });
                alertPopup.then(function (res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });

            }
        })

    };

    //varabile che esprime se deve essere cercata un'aula specifica
    $scope.selectedHole = false;
    if($rootScope.selectedSpecificHole != ""){
        $scope.selectedHole = true;
        $scope.findSpecificHole($rootScope.selectedSpecificHole);
    }

    //Lista delle aule trovate
    $scope.holes = [];
    //Lista per la singola aula cercata = []
    $scope.singleHolesResult = [];

    $scope.getItemHeight = function(item, index) {
        //Make evenly indexed items be 10px taller, for the sake of example
        return (index % 1) === 0 ? 50 : 60;
    };

    //rimuove l'aula specificata
    $scope.removeSelectedHole = function(){
        $rootScope.selectedSpecificHole = "";
        $scope.selectedHole = false
    }



}).controller('aboutController', function($scope,$ionicPopup){
        $scope.showLicensePopup = function(){
            var alertPopup = $ionicPopup.alert({
                title: 'Licenza!',
                template: "<p style='text-align: justify'>Copyright 2015 BaraccaSoftware development Inc.Licensed under the Apache License, Version 2.0 (the 'License'); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an 'AS IS' BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.</p>"
            });
            alertPopup.then(function (res) {
                console.log('Thank you for not eating my delicious ice cream cone');
                $rootScope.showInitialPopup = false;
            });
        }

    })
/**
 * Created by Lorenzo on 28/11/2014.
 */

var mysql      = require('mysql');
var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'lorenzo',
    password:'gordon',
    database: 'find_the_hole_db'

});
connection.connect();

exports.findAll = function(req, res) {
    res.send([{name:'wine1'}, {name:'wine2'}, {name:'wine3'}]);
};

exports.findById = function(req, res) {
    res.send({id:req.params.id, name: "The Name", description: "description"});
};

exports.prova = function(req,res){
    // select nomeAula FROM find_the_hole_db.aule,find_the_hole_db.lezioni WHERE lezioni.orarioFine >= "11:30" AND lezioni.aula = aule.idAula
    var query = "select * from find_the_hole_db.aule;";
    connection.query(query, function(err, rows, fields) {
        if (err){
            res.send(err);
        } else {
            res.send(rows);
        }


    });
}
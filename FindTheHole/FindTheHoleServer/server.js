/**
 * Created by Lorenzo on 28/11/2014.
 */

var express = require('express'),
    holes = require('./nodecellar/routes/holes.js');

var app = express();

app.get('/prova', holes.prova);
app.get('/wines/:id', holes.findById);

app.listen(3000);
console.log('Listening on port 3000...');


package com.baraccasoftware.findthehole.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.domain.HoleItem;

import java.util.ArrayList;

/**
 * Created by Lorenzo on 13/03/2015.
 */
public class SendFeedBackAdapter  extends BaseAdapter implements Filterable {
    private static ArrayList<HoleItem> holeItems;
    private static ArrayList<HoleItem> filtered;

    private LayoutInflater mInflater;

    public SendFeedBackAdapter(Context context, ArrayList<HoleItem> results){
        filtered = results;
        holeItems = results;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return filtered.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return filtered.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.hole_item_line_layout, null);
        }

        TextView holeName = (TextView) convertView.findViewById(R.id.holeName);
        holeName.setText("Nome aula: "+((HoleItem)getItem(position)).getHoleName());



        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                filtered= (ArrayList<HoleItem>) results.values;

                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<HoleItem> temp = new ArrayList<HoleItem>();
                for (int i = 0; i < getHoleItems().size();i++){
                    if(getHoleItems().get(i).getHoleName().contains(constraint.toString().toUpperCase())){
                        temp.add(getHoleItems().get(i));
                    }
                }

                results.count = temp.size();
                results.values = temp;

                return results;
            }
        };

        return filter;
    }

    public static ArrayList<HoleItem> getHoleItems() {
        return holeItems;
    }

    public static void setHoleItems(ArrayList<HoleItem> holeItems) {
        SendFeedBackAdapter.holeItems = holeItems;
    }
}



package com.baraccasoftware.findthehole.asynTask;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.baraccasoftware.findthehole.MainActivity;
import com.baraccasoftware.findthehole.adapter.HoleListAdapter;
import com.baraccasoftware.findthehole.adapter.LessonAdpater;
import com.baraccasoftware.findthehole.domain.HoleItem;
import com.baraccasoftware.findthehole.domain.Lesson;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Lorenzo on 12/03/2015.
 */
public class GetHoleLessonTask extends AsyncTask<String, String, String> {
    private ProgressDialog dialog;
    private ListView list;
    private MainActivity activity;

    public GetHoleLessonTask(MainActivity activity, ListView list) {
        dialog = new ProgressDialog(activity);
        this.list = list;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        dialog.setMessage("Ricerca lezioni in corso");
        dialog.show();
    }

    @Override
    protected String doInBackground(String... uri) {
        DefaultHttpClient httpClient = new DefaultHttpClient();

        int timeout = 10; // seconds
        HttpParams httpParams = httpClient.getParams();
        httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
        httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);
        HttpResponse response;
        String responseString = null;
        try {
            response = httpClient.execute(new HttpGet(uri[0]));
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            } else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        if(result!= null) {
            Object obj = null;
            try {
                ArrayList<Lesson> lessons = new ArrayList<Lesson>();
                JSONParser parser = new JSONParser();
                obj = parser.parse(result.replace("JSON_CALLBACK(", "").replace(");", ""));

                    JSONArray array = ((JSONArray) obj);
                if(array.size() == 0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Nessuna lezione prevista")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create().show();
                }else {
                    for (int i = 0; i < array.size(); i++) {
                        JSONObject object = ((JSONObject) array.get(i));
                        Lesson lesson = new Lesson(object.get("orario").toString());
                        lessons.add(lesson);
                    }
                    list.setAdapter(new LessonAdpater(activity, lessons));

                    Log.d("My App", obj.toString());
                }


            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
                if(((JSONObject) obj).containsKey("zeroResults")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Nessuna lezione prevista")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create().show();
                }
            }
        }else{
            Toast.makeText(activity, "Si e' verificato un errore", Toast.LENGTH_LONG).show();
        }

    }
}
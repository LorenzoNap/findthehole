package com.baraccasoftware.findthehole.asynTask;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.baraccasoftware.findthehole.MainActivity;
import com.baraccasoftware.findthehole.adapter.FreeHoleListAdapter;
import com.baraccasoftware.findthehole.adapter.ListFeedBackAdapter;
import com.baraccasoftware.findthehole.domain.FeedBack;
import com.baraccasoftware.findthehole.domain.FreeHoleItem;
import com.baraccasoftware.findthehole.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Lorenzo on 13/03/2015.
 */
public class GetFeedbackTask extends AsyncTask<String, String, String> {
    private ProgressDialog dialog;
    private ListView list;
    private MainActivity activity;

    public GetFeedbackTask(MainActivity activity, ListView list) {
        dialog = new ProgressDialog(activity);
        this.list = list;
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        dialog.setMessage("Ricerca segnalazioni...");
        dialog.show();
    }

    @Override
    protected String doInBackground(String... uri) {
        DefaultHttpClient httpClient = new DefaultHttpClient();

        int timeout = 10; // seconds
        HttpParams httpParams = httpClient.getParams();
        httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
        httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);
        HttpResponse response;
        String responseString = null;
        try {
            response = httpClient.execute(new HttpGet(uri[0]));
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            } else {
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            return null;
        } catch (IOException e) {
            return  null;
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        if(result != null) {
            try {
                ArrayList<FeedBack> feedBacks = new ArrayList<FeedBack>();
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(result.replace("JSON_CALLBACK(", "").replace(");", ""));
                JSONObject results = ((JSONObject) obj);
                JSONArray array = (JSONArray) results.get("feedback");
                for (int i = 0; i < array.size(); i++) {
                    JSONObject object = ((JSONObject) array.get(i));
                    FeedBack feedBack = new FeedBack(object.get("aula").toString(), object.get("stato").toString(), Utils.getCurrentiTimeFromDate(object.get("orario").toString()));
                    feedBacks.add(feedBack);
                }
                if(array.size() == 0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Nessuna segnalazione inviata")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                }
                            });
                    // Create the AlertDialog object and return it
                     builder.create().show();
                }
                list.setAdapter(new ListFeedBackAdapter(activity, feedBacks));

                Log.d("My App", obj.toString());

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
            }
        }else{
            Toast.makeText(activity, "Si e' verificato un errore", Toast.LENGTH_LONG).show();
        }

    }
}

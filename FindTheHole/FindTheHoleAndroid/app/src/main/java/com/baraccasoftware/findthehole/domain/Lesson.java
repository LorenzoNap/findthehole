package com.baraccasoftware.findthehole.domain;

/**
 * Created by Lorenzo on 12/03/2015.
 */
public class Lesson {

    private String lessonHour;


    public Lesson(String lessonHour) {
        this.lessonHour = lessonHour;
    }

    public String getLessonHour() {
        return lessonHour;
    }

    public void setLessonHour(String lessonHour) {
        this.lessonHour = lessonHour;
    }
}

package com.baraccasoftware.findthehole.domain;

/**
 * Created by Lorenzo on 09/03/2015.
 */
public class FreeHoleItem {

    private String holeName;
    private String nextLesson;

    public FreeHoleItem(String holeName, String nextLesson) {
        this.holeName = holeName;
        this.nextLesson = nextLesson;
    }

    public FreeHoleItem(String holeName) {
        this.holeName = holeName;
    }


    public String getNextLesson() {
        return nextLesson;
    }

    public void setNextLesson(String nextLesson) {
        this.nextLesson = nextLesson;
    }

    public String getHoleName() {
        return holeName;
    }

    public void setHoleName(String holeName) {
        this.holeName = holeName;
    }
}

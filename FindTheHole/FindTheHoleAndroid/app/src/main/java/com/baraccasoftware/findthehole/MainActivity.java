package com.baraccasoftware.findthehole;

import android.app.Activity;

import android.app.ActionBar;

import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.baraccasoftware.findthehole.adapter.HoleListAdapter;
import com.baraccasoftware.findthehole.fragment.AddLessonFragment;
import com.baraccasoftware.findthehole.fragment.HomeFragment;
import com.baraccasoftware.findthehole.fragment.InfoFragment;
import com.baraccasoftware.findthehole.fragment.SelectHoleFragment;
import com.baraccasoftware.findthehole.fragment.SendFeedBackFragment;


public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, HomeFragment.OnFragmentInteractionListener,SelectHoleFragment.OnFragmentInteractionListener,AddLessonFragment.OnFragmentInteractionListener,SendFeedBackFragment.OnFragmentInteractionListener,InfoFragment.OnFragmentInteractionListener {

    private DrawerLayout mDrawerLayout;


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        android.app.FragmentManager fragmentManager = getFragmentManager() ;
        switch (position) {
            case 0:
                mTitle = getString(R.string.home);
                HomeFragment homeFragment = HomeFragment.newInstance(position+1);
                Bundle args = new Bundle();
                args.putBoolean(HomeFragment.SEARCH_FREE_HOLE, true);
                homeFragment.setArguments(args);

                fragmentManager.beginTransaction()
                        .replace(R.id.container, homeFragment)
                        .commit();
                break;
            case 1:
                mTitle = getString(R.string.search_hole);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, SelectHoleFragment.newInstance())
                        .commit();
                break;
            case 2:
                mTitle = getString(R.string.sendFeedback);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, SendFeedBackFragment.newInstance())
                        .commit();
                break;
            case 3:
                mTitle = getString(R.string.segnalaOrario);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, AddLessonFragment.newInstance())
                        .commit();
                break;
            case 4:
                mTitle = getString(R.string.about);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, InfoFragment.newInstance())
                        .commit();
                break;
        }


        // update the main content by replacing fragments

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.home);
                break;
            case 2:
                mTitle = getString(R.string.search_hole);
                break;
            case 3:
                mTitle = getString(R.string.sendFeedback);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }*/

  /*  @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/




    @Override
    public void onSelectHoleFragment(int position) {

        SelectHoleFragment selectHoleFragment = new SelectHoleFragment();
       /* Bundle args = new Bundle();
        args.putInt(ArticleFragment.ARG_POSITION, position);
        newFragment.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, selectHoleFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();



    }

    @Override
    public void openSendFeedBackFragment() {
        SendFeedBackFragment sendFeedBackFragment = SendFeedBackFragment.newInstance();
       /* Bundle args = new Bundle();
        args.putInt(ArticleFragment.ARG_POSITION, position);
        newFragment.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, sendFeedBackFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }


    @Override
    public void onSelectedHoleName(String holeName) {
        HomeFragment homeFragment = HomeFragment.newInstance(holeName);
       /* Bundle args = new Bundle();
        args.putInt(ArticleFragment.ARG_POSITION, position);
        newFragment.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, homeFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }

    @Override
    public void onSendFeedbackFragmentInteraction(Uri uri) {
        SendFeedBackFragment sendFeedBackFragment = SendFeedBackFragment.newInstance();
       /* Bundle args = new Bundle();
        args.putInt(ArticleFragment.ARG_POSITION, position);
        newFragment.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, sendFeedBackFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        InfoFragment infoFragment = InfoFragment.newInstance();
       /* Bundle args = new Bundle();
        args.putInt(ArticleFragment.ARG_POSITION, position);
        newFragment.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, infoFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    @Override
    public void openAddLesson() {
        AddLessonFragment addLessonFragment = AddLessonFragment.newInstance();
       /* Bundle args = new Bundle();
        args.putInt(ArticleFragment.ARG_POSITION, position);
        newFragment.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.container, addLessonFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }
}

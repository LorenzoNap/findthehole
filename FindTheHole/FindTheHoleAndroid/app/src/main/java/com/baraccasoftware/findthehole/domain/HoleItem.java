package com.baraccasoftware.findthehole.domain;

/**
 * Created by Lorenzo on 10/03/2015.
 */
public class HoleItem {

    private String holeName;

    public HoleItem(String holeName) {
        this.holeName = holeName;
    }

    public String getHoleName() {
        return holeName;
    }

    public void setHoleName(String holeName) {
        this.holeName = holeName;
    }
}

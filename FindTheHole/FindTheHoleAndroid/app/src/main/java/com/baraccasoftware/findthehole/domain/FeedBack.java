package com.baraccasoftware.findthehole.domain;

/**
 * Created by Lorenzo on 13/03/2015.
 */
public class FeedBack {

    private String holeName;
    private String status;
    private String feedBackHour;

    public FeedBack(String holeName, String status, String feedBackHour) {
        this.holeName = holeName;
        this.status = status;
        this.feedBackHour = feedBackHour;
    }


    public String getHoleName() {
        return holeName;
    }

    public void setHoleName(String holeName) {
        this.holeName = holeName;
    }

    public String getStatus() {
        if(status.equals("true")) return "Libera";
        return "Occupata";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeedBackHour() {
        return feedBackHour;
    }

    public void setFeedBackHour(String feedBackHour) {
        this.feedBackHour = feedBackHour;
    }
}

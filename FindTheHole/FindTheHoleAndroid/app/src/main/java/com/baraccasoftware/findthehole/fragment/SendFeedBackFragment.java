package com.baraccasoftware.findthehole.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.baraccasoftware.findthehole.MainActivity;
import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.adapter.HoleListAdapter;
import com.baraccasoftware.findthehole.asynTask.GetHoleNameTask;
import com.baraccasoftware.findthehole.dialog.SendFeedbackDialog;
import com.baraccasoftware.findthehole.domain.HoleItem;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SendFeedBackFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SendFeedBackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SendFeedBackFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final String ARG_HOLE_NAME = "hole_name";
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
          * @return A new instance of fragment SendFeedBackFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SendFeedBackFragment newInstance() {
        SendFeedBackFragment fragment = new SendFeedBackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_HOLE_NAME, null);
        /*args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(args);
        return fragment;
    }

    public SendFeedBackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_send_feed_back, container, false);
        EditText editText = (EditText) rootView.findViewById(R.id.filterHole);


        //HoleName ListView
        final ListView listView = (ListView) rootView.findViewById(R.id.holeNameListView);
        GetHoleNameTask getHoleNameTask= new GetHoleNameTask((MainActivity) getActivity(),listView);
        getHoleNameTask.execute("http://findthehole-baraccasoftware.rhcloud.com/getAllHoles?callback=JSON_CALLBACK");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object o = listView.getItemAtPosition(i);
                HoleItem holeItem =(HoleItem)o;//As you are using Default String Adapter
                //mListener.onSelectedHoleName(holeItem.getHoleName());
                SendFeedbackDialog sendFeedbackDialog =  SendFeedbackDialog.newInstance(holeItem.getHoleName());
                sendFeedbackDialog.show(getFragmentManager(),"Pluto");
            }
        });



        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ((HoleListAdapter)listView.getAdapter()).getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {}
        });
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSendFeedbackFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onSendFeedbackFragmentInteraction(Uri uri);
    }

}

package com.baraccasoftware.findthehole.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.domain.FreeHoleItem;
import com.baraccasoftware.findthehole.domain.HoleItem;

import java.util.ArrayList;

/**
 * Created by Lorenzo on 13/03/2015.
 */
public class DrawerAdapter extends BaseAdapter {
    private static ArrayList<String> items;

    private LayoutInflater mInflater;

    public DrawerAdapter(Context context, ArrayList<String> results) {
        items = results;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return items.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.drawer_line_layout, null);

        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.logo);
        TextView holeName = (TextView) convertView.findViewById(R.id.item);
        holeName.setText(((String) getItem(position)));

        if(getItem(position).equals(mInflater.getContext().getResources().getString(R.string.home)) ){
            imageView.setImageResource(R.drawable.ic_home );
        }
        if(getItem(position).equals(mInflater.getContext().getResources().getString(R.string.search_hole))){

            imageView.setImageResource(R.drawable.ic_search_black );
        }

        if(getItem(position).equals(mInflater.getContext().getResources().getString(R.string.sendFeedback))){

            imageView.setImageResource( R.drawable.ic_arrow_up_c_black );
        }
        if(getItem(position).equals(mInflater.getContext().getResources().getString(R.string.segnalaOrario))){

            imageView.setImageResource( R.drawable.ic_compose );
        }
        if(getItem(position).equals(mInflater.getContext().getResources().getString(R.string.info))){

            imageView.setImageResource( R.drawable.ic_more );
        }


        return convertView;
    }
}
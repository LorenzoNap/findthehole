package com.baraccasoftware.findthehole.fragment;

import android.app.Activity;
import android.app.AlertDialog;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.asynTask.SendLessonTask;
import com.baraccasoftware.findthehole.dialog.TimePickerFragment;
import com.baraccasoftware.findthehole.utils.MultiSelectionSpinner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddLessonFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddLessonFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddLessonFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String startLessonTime;
    private String endLessonTime;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment AddLessonFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddLessonFragment newInstance() {
        AddLessonFragment fragment = new AddLessonFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    public AddLessonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView =  inflater.inflate(R.layout.fragment_add_lesson, container, false);

        final MultiSelectionSpinner spinner = (MultiSelectionSpinner) rootView.findViewById(R.id.spinnerSelectDay);
        // Create an ArrayAdapter using the string array and a default spinner layout
        String[] array = { "Lunedì", "Martedì", "Mercoledì","Giovedì","Venerdì" };
        spinner.setItems(array);

        final EditText editText = (EditText) rootView.findViewById(R.id.holeName);

        final TimePickerFragment newFragment1 = new TimePickerFragment();
        final TimePickerFragment newFragment2 = new TimePickerFragment();
        Button startLesson = (Button) rootView.findViewById(R.id.buttonStartLesson);
        startLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newFragment1.show(getActivity().getFragmentManager(), "timePicker");
            }
        });

        final Button endLesson = (Button) rootView.findViewById(R.id.buttonOrarioFine);
        endLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newFragment2.show(getActivity().getFragmentManager(), "timePicker");
            }
        });

        Button sendLesson = (Button) rootView.findViewById(R.id.buttonSalvaOrario);
        sendLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               String listDays = spinner.getSelectedItemsAsString();
               startLessonTime =newFragment1.getDate();
               endLessonTime = newFragment2.getDate();
               String holeName = editText.getText().toString();

                String error = "";
               //Check params
               if(holeName.equals("")){
                   error += "Inserisci nome aula"+"\n";

               }
               if( listDays.equals("")){
                   error += "Seleziona i giorni della lezione"+"\n";
               }
               if(startLessonTime == null || endLessonTime == null){
                   error += "Inserisci orari lezione"+"\n";
               }

                if(error.equals("")){
                    Uri.Builder builder = new Uri.Builder();
                    builder
                            .appendQueryParameter("dayOfWeek", listDays)
                            .appendQueryParameter("holeName", holeName)
                            .appendQueryParameter("endLesson",endLessonTime)
                            .appendQueryParameter("startLesson",startLessonTime);

                    String myUrl = builder.build().toString().replace("?","");
                    SendLessonTask sendLessonTask = new SendLessonTask((com.baraccasoftware.findthehole.MainActivity) getActivity());
                    sendLessonTask.execute("http://findthehole-baraccasoftware.rhcloud.com/sendLessonChange?callback=JSON_CALLBACK&"+myUrl);
                }
                else{
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }



            }
        });
        return rootView;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_lesson_menu, menu);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void openAddLesson();
    }

}

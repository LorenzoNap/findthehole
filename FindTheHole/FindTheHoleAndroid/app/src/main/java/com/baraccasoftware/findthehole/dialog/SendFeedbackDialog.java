package com.baraccasoftware.findthehole.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Toast;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.asynTask.SendFeedbackTask;
import com.baraccasoftware.findthehole.domain.HoleItem;
import com.baraccasoftware.findthehole.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Lorenzo on 13/03/2015.
 */
public class SendFeedbackDialog extends DialogFragment {
    private static final String ARG_HOLE_NAME = "hole";
    private boolean choice;
    private HoleItem holeItem;

    public SendFeedbackDialog() {
    }

    public static SendFeedbackDialog newInstance (String holeName) {
        SendFeedbackDialog fragment = new SendFeedbackDialog();
        Bundle args = new Bundle();

        args.putString(ARG_HOLE_NAME, holeName);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Resources res = getResources();
        final String[] choices = res.getStringArray(R.array.hole_status);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the dialog title
        builder.setTitle(R.string.send_feedback_dialog_title)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(R.array.hole_status, -1,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == 0){
                                    choice = true;
                                }else{
                                    choice = false;
                                }

                            }
                        })
                        // Set the action buttons
                .setPositiveButton(R.string.send_feedback_dialog_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, so save the mSelectedItems results somewhere
                        // or return them to the component that opened the dialog
                        Bundle bundle = getArguments();
                        HoleItem holeItem1 = new HoleItem(bundle.getString(ARG_HOLE_NAME));
                        SendFeedbackTask sendFeedbackTask = new SendFeedbackTask((com.baraccasoftware.findthehole.MainActivity) getActivity());

                        sendFeedbackTask.execute("http://findthehole-baraccasoftware.rhcloud.com/sendHoleFeedBack?callback=JSON_CALLBACK&status=%7B%22hole%22:%22"+holeItem1.getHoleName()+"%22,%22status%22:"+choice+"%7D&time="+ Utils.getCurrentiTime());

                    }
                })
                .setNegativeButton(R.string.cancel_feedback_dialog_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }
}

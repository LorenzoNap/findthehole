package com.baraccasoftware.findthehole.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.domain.FeedBack;
import com.baraccasoftware.findthehole.domain.Lesson;

import java.util.ArrayList;

/**
 * Created by Lorenzo on 13/03/2015.
 */
public class ListFeedBackAdapter extends BaseAdapter {
    private static ArrayList<FeedBack> lessons;

    private LayoutInflater mInflater;

    public ListFeedBackAdapter(Context context, ArrayList<FeedBack> results) {
        lessons = results;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lessons.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return lessons.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.feedback_line_layout, null);
        }
            TextView holeName = (TextView) convertView.findViewById(R.id.holeName);
            holeName.setText("Aula: " + ((FeedBack) getItem(position)).getHoleName());

            TextView status = (TextView) convertView.findViewById(R.id.hole_status);
            status.setText("Stato: " + ((FeedBack) getItem(position)).getStatus());

            TextView hour = (TextView) convertView.findViewById(R.id.feedback_hour);
            hour.setText("Orario segnalazione: " + ((FeedBack) getItem(position)).getFeedBackHour());

        return convertView;
    }
}

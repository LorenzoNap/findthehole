package com.baraccasoftware.findthehole.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.domain.FreeHoleItem;
import com.baraccasoftware.findthehole.domain.Lesson;

import java.util.ArrayList;

/**
 * Created by Lorenzo on 12/03/2015.
 */
public class LessonAdpater extends BaseAdapter {
    private static ArrayList<Lesson> lessons;

    private LayoutInflater mInflater;

    public LessonAdpater(Context context, ArrayList<Lesson> results) {
        lessons = results;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lessons.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return lessons.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.lesson_line_layout, null);


        }

        TextView holeName = (TextView) convertView.findViewById(R.id.lessonHour);
        holeName.setText("Orario lezione: " + ((Lesson) getItem(position)).getLessonHour());


        return convertView;
    }
}

package com.baraccasoftware.findthehole.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.baraccasoftware.findthehole.MainActivity;
import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.asynTask.GetFeedbackTask;
import com.baraccasoftware.findthehole.asynTask.GetFreeHolesTask;
import com.baraccasoftware.findthehole.asynTask.GetHoleLessonTask;
import com.baraccasoftware.findthehole.dialog.ShowIntialPopupDialog;
import com.baraccasoftware.findthehole.domain.FeedBack;
import com.baraccasoftware.findthehole.domain.FreeHoleItem;
import com.baraccasoftware.findthehole.utils.Utils;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends Fragment {


    private static final int SHOW_INITIAL_POPUP = 1;
    public static final String SEARCH_FREE_HOLE = "Search Free Hole";
    private OnFragmentInteractionListener mListener;

    public interface OnFragmentInteractionListener {
        public void onSelectHoleFragment(int position);
        public void openSendFeedBackFragment();
    }


    private String holeName;

    private Button findHolesButton;
    private Button searchHoleButton;
    private Button sendFeedbackButton;
    private Button getFeedbacksButton;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_HOLE_NAME = "hole_name";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HomeFragment newInstance(int sectionNumber) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_HOLE_NAME, null);
        fragment.setArguments(args);
        return fragment;
    }

    public static HomeFragment newInstance(String holeName) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_HOLE_NAME, holeName);
        fragment.setArguments(args);
        return fragment;
    }





    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        //FreeHoles ListView
        final ListView listView = (ListView) rootView.findViewById(R.id.HoleListView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = listView.getItemAtPosition(position);
                if(o instanceof FreeHoleItem){
                    FreeHoleItem freeHoleItem =(FreeHoleItem)o;//As you are using Default String Adapter
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Prossima Lezione: "+freeHoleItem.getNextLesson())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                }
                            });
                    // Create the AlertDialog object and return it
                     builder.create().show();
                }
                if(o instanceof FeedBack){
                    FeedBack feedBack = (FeedBack)o;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Stato: "+feedBack.getStatus()+" Orario segnalazione: "+feedBack.getFeedBackHour())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create().show();
                }

            }
        });


        Context context = getActivity();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Boolean showInitiliaPoup = sharedPref.getBoolean(String.valueOf(R.string.preference_intial_popup), true);
        if(showInitiliaPoup){
            ShowIntialPopupDialog showIntialPopupDialog = new ShowIntialPopupDialog();
            showIntialPopupDialog.show(getFragmentManager(),"popup");
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(String.valueOf(R.string.preference_intial_popup), false);
            editor.commit();
        }

        //Main buttons
        findHolesButton = (Button) rootView.findViewById(R.id.searchFreeHoleButton);
        searchHoleButton = (Button) rootView.findViewById(R.id.searchHoleButton);
        sendFeedbackButton = (Button) rootView.findViewById(R.id.sendFeedbackButton);
        getFeedbacksButton = (Button) rootView.findViewById(R.id.getFeedbacksButton);

        Bundle bundle = getArguments();
        if(bundle.getBoolean(SEARCH_FREE_HOLE) == true){
            GetFreeHolesTask getFreeHolesTask = new GetFreeHolesTask((MainActivity) getActivity(),listView);
            getFreeHolesTask.execute("http://findthehole-baraccasoftware.rhcloud.com/findHoles?callback=JSON_CALLBACK&time="+ Utils.getCurrentiTime());
        }
        //SetOnClickListener
        findHolesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetFreeHolesTask getFreeHolesTask = new GetFreeHolesTask((MainActivity) getActivity(),listView);
                getFreeHolesTask.execute("http://findthehole-baraccasoftware.rhcloud.com/findHoles?callback=JSON_CALLBACK&time="+ Utils.getCurrentiTime());
            }
        });

        searchHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onSelectHoleFragment(1);
            }
        });



        if (bundle.get(ARG_HOLE_NAME) != null){

            GetHoleLessonTask getHoleLessonTask = new GetHoleLessonTask((MainActivity) getActivity(),listView);
            getHoleLessonTask.execute("http://findthehole-baraccasoftware.rhcloud.com/findSpecificHole?callback=JSON_CALLBACK&hole=%7B%22aula%22:%22"+bundle.get(ARG_HOLE_NAME)+"%22%7D");
            //getActivity().getIntent().removeExtra("key");
            bundle.clear();

        }

        sendFeedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.openSendFeedBackFragment();
            }
        });


        getFeedbacksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetFeedbackTask getFeedbackTask = new GetFeedbackTask((MainActivity) getActivity(),listView);
                getFeedbackTask.execute("http://findthehole-baraccasoftware.rhcloud.com/getHolesFeedBack?callback=JSON_CALLBACK");
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }




}



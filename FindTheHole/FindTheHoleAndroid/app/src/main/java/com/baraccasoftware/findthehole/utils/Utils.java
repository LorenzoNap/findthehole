package com.baraccasoftware.findthehole.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Lorenzo on 11/03/2015.
 */
public class Utils {

    public static String getCurrentiTime(){
        Date date = new Date();
        String curTime = String.format("%02d:%02d", date.getHours(), date.getMinutes());

        return curTime;
    }

    public static String getCurrentiTimeFromDate(String date){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date temp = formatter.parse(date.replace("T"," "));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(temp);
            calendar.add(Calendar.HOUR, 2);
            temp = calendar.getTime();

            String curTime = String.format("%02d:%02d", temp.getHours(), temp.getMinutes());
            return curTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}

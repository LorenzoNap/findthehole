package com.baraccasoftware.findthehole.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.baraccasoftware.findthehole.R;
import com.baraccasoftware.findthehole.domain.FreeHoleItem;

import java.util.ArrayList;

/**
 * Created by Lorenzo on 09/03/2015.
 */
public class FreeHoleListAdapter extends BaseAdapter {
    private static ArrayList<FreeHoleItem> holeItems;

    private LayoutInflater mInflater;

    public FreeHoleListAdapter(Context context, ArrayList<FreeHoleItem> results){
        holeItems = results;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return holeItems.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return holeItems.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.free_hole_line_layout, null);


         }

        TextView holeName = (TextView) convertView.findViewById(R.id.holeName);
        holeName.setText("Aula libera: "+((FreeHoleItem)getItem(position)).getHoleName());

        TextView nextLesson = (TextView) convertView.findViewById(R.id.nextLesson);
        nextLesson.setText("Prossima lezione: "+((FreeHoleItem)getItem(position)).getNextLesson());

        return convertView;
    }

}
